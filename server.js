//version inicial

var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000;

var path = require('path');

var bodyParser = require('body-parser');
app.use(bodyParser.json());

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

var movimientosJSON = require('./movimientosV2.json');

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'api/index.html'));
    //res.send("NodeJS");
});

app.get('/clientes', (req, res) => {
    res.send('Aqui van los clientes devueltos nuevos');
});

app.get('/clientes/:idCliente', (req, res) => {
    res.send(`Aqui tiene al cliente numero:  ${req.params.idCliente}`);
});


app.get('/Movimientos/v1', (req, res) => {
    res.sendfile('movimientosV1.json');
});

app.get('/Movimientos/v2', (req, res) => {
    res.send(movimientosJSON);
});

app.get('/Movimientosq/v2', (req, res) => {
    console.log(req.query);
    res.send('Recibido');
});



app.post('/', (req, res) => {
    res.send("Hemos recibido su petición POST");
});

app.post('/Movimientos/v2', (req, res) => {
    var nuevo = req.body;
    nuevo.id = movimientosJSON.length + 1;
    movimientosJSON.push(nuevo);
    console.log(nuevo);
    res.send('Movimiendo dado de alta');
});
